<?
	include("conexion.php"); // <--- Cambiar el archivo de conexion
	session_destroy(); 
?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control Administrativo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- styles -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>

<body>
  <header>
    <!-- start top -->
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">

          <div class="logo">
            <a href="index.php"><img src="assets/img/logo_jaguares.jpg" height="60" width="60" alt=""  /></a>
          </div>
		  

          <!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <!-- end top -->
  </header>
  <!-- section intro -->
  <section align="center">
      <h1>Sistema de Control Administrativo</h1>
      <h2>Jaguares, AC</h2>
	  <h3>Acceso al Sistema</h3>
	  <h4>Para un desempeño óptimo de la plataforma favor de usar el navegador Chrome <span><img src="assets/img/chrome.png" height="30" width="30"</span></h4>
	  <div>
          <form method="post" action="dbcrud1.php">
            <div class="row">
              <div class="col-lg-4 field">
                <input type="text" name="name" class="form-control" id="name" placeholder="usuario" required /><br>
				<input type="password" name="pwd" class="form-control" placeholder="contraseña" required /><br>
				<input type="submit" class="form-control btn btn-success btn" id="userlogin" name="userlogin" value="Acceder" />
			   </div>&nbsp; &nbsp; 
			  <br><br>
            </div>
          </form>
	  </div>
  </section>  
  <!-- end intro -->
  <!-- Section about -->
  <section id="about" class="section">
    <div class="container">
    </div>
  </section>
  <!-- end section about -->
  <footer>
    <div class="verybottom">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="aligncenter">
			  
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Vesperr
                -->
                Developed by <a href="mailto:jmunoz@syner.info">C-Solutions</a> with template created by <a href="https://bootstrapmade.com/">BootstrapMade.com</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- Javascript Library Files -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.easing.js"></script>
  <script src="assets/js/bootstrap.js"></script>
  <script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
  <script src="assets/js/nagging-menu.js"></script>
  <script src="assets/js/jquery.nav.js"></script>
  <script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
  <script src="assets/js/portfolio/jquery.quicksand.js"></script>
  <script src="assets/js/portfolio/setting.js"></script>
  <script src="assets/js/hover/jquery-hover-effect.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/animate.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom Javascript File -->
  <script src="assets/js/custom.js"></script>

</body>

</html>
