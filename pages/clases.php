<?php
	class Evento {
		///Propiedades
		public $id_evento = false;
		public $nombre_evento = false;
		public $fecha_evento = false;
		public $hora_inicio = false;
		public $hora_salida = false;
		
		///Metodos
		function Get_id_evento(){
			if($this->id_evento !==false) return $this->id_evento;
		}
		
		function Get_nombre_evento(){
			if($this->nombre_evento !==false) return $this->nombre_evento;
		}

		function Get_fecha_evento(){
			if($this->fecha_evento !==false) return $this->fecha_evento;
		}

		function Get_hora_inicio(){
			if($this->hora_inicio !==false) return $this->hora_inicio;
		}

		function Get_hora_salida(){
			if($this->hora_salida !==false) return $this->hora_salida;
		}
		
	}
?>